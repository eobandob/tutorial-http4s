package com.eduardoobando

import cats._
import cats.effect._
import cats.implicits._
import org.http4s.circe._
import org.http4s._
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.dsl._
import org.http4s.dsl.impl._
import org.http4s.headers._
import org.http4s.implicits._
import org.http4s.server._

import java.time.Year
import scala.collection.mutable
import scala.util.Try

object Http4sTutorial extends IOApp {
  import Classes._
  import Data._

  /*
    - GET all movies for a director under a given year
    - GET all actors for a movie
    - GET details about a director
    - POST add a new director 
   */

  implicit val yearQueryParamDecoder: QueryParamDecoder[Year] =
    QueryParamDecoder[Int].emap { yearInt =>
      Try(Year.of(yearInt))
        .toEither
        .leftMap( e => ParseFailure(e.getMessage, "Details of the error") )
    }
  object DirectQueryParamMatcher extends QueryParamDecoderMatcher[String]("director")
  object YearQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[Year]("year")
  /*
    - GET all movies for a director under a given year
    GET /movies?director=Zack%20Snyder&year=2021

    - GET all actors for a movie
    GET /movies/<uuid>/actors
   */
  def movieRoutes[F[_]: Monad]: HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._
    HttpRoutes.of[F] {
      case GET -> Root / "movies" :? DirectQueryParamMatcher(director) +& YearQueryParamMatcher(maybeYear) =>
        maybeYear match {
          case Some(validatedYear) =>
            validatedYear.fold(
              _ => BadRequest("The year was badly formatted"),
              year => {
                val moviesByDirectorAndYear = findMoviesByDirector(director).filter(_.year == year)
                Ok(moviesByDirectorAndYear.asJson)
              }
            )
          case None => Ok(findMoviesByDirector(director).asJson)
        }
      case GET -> Root / "movies" / UUIDVar(movieId) / "actors" =>
        findMovieById(movieId).map(_.actors) match {
          case Some(actors) => Ok(actors.asJson)
          case None => NotFound(s"No movie with if $movieId found in the database")
        }
    }
  }

  object DirectorPath {
    def unapply(str: String): Option[Director] = {
      Try {
        val tokens = str.split(" ")
        Director(tokens(0), tokens(1))
      }.toOption
    }
  }

  /*
    - GET details about a director
    GET /directors/Zack%20Snyder
    - POST add a new director
   */
  def directorRoutes[F[_]: Monad]: HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._
    HttpRoutes.of[F] {
      case GET -> Root / "directors" / DirectorPath(director) =>
        findDirectorDetailsByDirector(director) match {
          case Some(dirDetails) => Ok(dirDetails.asJson)
          case _ => NotFound(s"No director '$director' found")
        }

    }
  }

  def allRoutes[F[_]: Monad]: HttpRoutes[F] = movieRoutes[F] <+> directorRoutes[F]

  def allRoutesComplete[F[_]: Monad]: HttpApp[F] = allRoutes[F].orNotFound

  override def run(args: List[String]): IO[ExitCode] = {
    val apis = Router(
      "/api" -> movieRoutes[IO],
      "/api/admin" -> directorRoutes[IO]
    ).orNotFound
    BlazeServerBuilder[IO](runtime.compute)
      .bindHttp(8080, "localhost")
      .withHttpApp(allRoutesComplete)   // alternative use variable apis
      .resource
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }

}
