package com.eduardoobando

object Classes {

  // movie database
  type Actor = String
  case class Movie(id: String, title: String, year: Int, actors: List[Actor], director: String)
  case class Director(firstName: String, lastName: String){
    override def toString: Actor = s"$firstName $lastName"
  }
  case class DirectorDetails(firstName: String, lastName: String, genre: String)
  object DirectorDetails {
    def fromDirector(director: Director)(genre: String): DirectorDetails =
      DirectorDetails(director.firstName, director.lastName, genre)
  }

}
